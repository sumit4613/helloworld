# Apli.ai Task 1 -> Creation of Feedback form

## To run this in your machine  

### For windows machine

- Upgrade pip 
``` bash
pip install --user --upgrade pip 
```

- Go to desired folder and create a virtualenv 
```bash 
virtualenv -p python . 
```

- Activate the virtual environment 
``` bash
./Scripts/activate 
```

- Install the dependencies
```bash 
pip install requirements.txt
```

- Make migrations and create the database
```bash
python manage.py makemigrations
python manage.py migrate 
```

- Run the server
```bash 
python manage.py runserver 
```

Now, open this url in your browser http://127.0.0.1:8000/

### For linux machine

- Upgrade pip
```bash
python3 -m pip install --user --upgrade pip
```

- Install the virtualenv
```bash
python3 -m pip install --user virtualenv
```

- Creating a virtualenv
```bash
python3 -m virtualenv env
```

- Activating a virtualenv
```bash
source env/bin/activate
```

- Install the dependencies
```bash
pip install requirements.txt
```
- Make migrations and create the database
```bash
python3 manage.py makemigrations
python3 manage.py migrate 
```

- Run the server
```bash
python3 manage.py runserver 
```

Now, open this url in your browser http://127.0.0.1:8000
